package com.slayend.fonoapp.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
    @NotNull(message = "UserDTO.email No puede ser null")
    @NotBlank(message = "UserDTO.email No puede estar vacio")
    private String email;
    @NotNull(message = "UserDTO.username No puede ser null")
    @NotBlank(message = "UserDTO.username No puede estar vacio")
    private String username;
    @NotNull(message = "UserDTO.password No puede ser null")
    @NotBlank(message = "UserDTO.password No puede estar vacio")
    private String password;
    private List<RolDTO> rols;
}
