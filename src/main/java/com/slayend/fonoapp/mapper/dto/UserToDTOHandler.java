package com.slayend.fonoapp.mapper.dto;

import com.slayend.fonoapp.dto.RolDTO;
import com.slayend.fonoapp.dto.UserDTO;
import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.mapper.support.AbstractHandler;

public class UserToDTOHandler extends AbstractHandler {


    @Override
    public boolean isSourceSupported(Object object) {
        return object instanceof Customer;
    }

    @Override
    protected <T> T doMap(Object object, Class<T> target, Mapper mapper) {
        Customer entity = (Customer) object;
        UserDTO dto = new UserDTO();
        dto.setEmail(entity.getEmail());
        dto.setUsername(entity.getUsername());
        dto.setRols(mapper.mapSetToList(entity.getAuthorities(), RolDTO.class));
        return (T) dto;
    }

}
