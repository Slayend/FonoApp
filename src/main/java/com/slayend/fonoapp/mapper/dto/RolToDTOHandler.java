package com.slayend.fonoapp.mapper.dto;

import com.slayend.fonoapp.dto.RolDTO;
import com.slayend.fonoapp.entity.Authority;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.mapper.support.AbstractHandler;

public class RolToDTOHandler extends AbstractHandler {


    @Override
    public boolean isSourceSupported(Object object) {
        return object instanceof Authority;
    }

    @Override
    protected <T> T doMap(Object object, Class<T> target, Mapper mapper) {
        Authority entity = (Authority) object;
        RolDTO dto = new RolDTO();
        dto.setNombre(entity.getNombre());
        return (T) dto;
    }

}
