package com.slayend.fonoapp.mapper;

import com.slayend.fonoapp.dto.RolDTO;
import com.slayend.fonoapp.dto.UserDTO;
import com.slayend.fonoapp.entity.Authority;
import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.mapper.dto.RolToDTOHandler;
import com.slayend.fonoapp.mapper.dto.UserToDTOHandler;
import com.slayend.fonoapp.mapper.entity.RolToEntityHandler;
import com.slayend.fonoapp.mapper.entity.UserToEntityHandler;
import com.slayend.fonoapp.mapper.support.MapperImpl;


public class CatalogoHandlers {

    private CatalogoHandlers() {
    }

    public static void setHandlers(MapperImpl mapper) {
        //Convertir entity en dto
        mapper.addHandler(new UserToDTOHandler(), UserDTO.class);
        mapper.addHandler(new RolToDTOHandler(), RolDTO.class);

        //Convertir DTO a entity
        mapper.addHandler(new UserToEntityHandler(), Customer.class);
        mapper.addHandler(new RolToEntityHandler(), Authority.class);


    }
}
