package com.slayend.fonoapp.mapper;

import java.util.List;
import java.util.Set;

public interface Mapper {

    <T> T map(Object object, Class<T> target);

    <T> List<T> mapList(List<?> list, Class<T> itemTarget);

    <T> Set<T> mapListToSet(List<?> list, Class<T> itemTarget);

    <T> List<T> mapSetToList(Set<?> list, Class<T> itemTarget);

}
