package com.slayend.fonoapp.mapper.entity;

import com.slayend.fonoapp.dto.UserDTO;
import com.slayend.fonoapp.entity.Authority;
import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.mapper.support.AbstractHandler;

public class UserToEntityHandler extends AbstractHandler {

    @Override
    public boolean isSourceSupported(Object object) {
        return object instanceof UserDTO;
    }

    @Override
    protected <T> T doMap(Object object, Class<T> target, Mapper mapper) {
        UserDTO dto = (UserDTO) object;
        Customer entity = new Customer();
        entity.setEmail(dto.getEmail());
        entity.setUsername(dto.getUsername());
        entity.setPassword(dto.getPassword());
        entity.setAuthorities(mapper.mapListToSet(dto.getRols(), Authority.class));
        return (T) entity;
    }
}
