package com.slayend.fonoapp.mapper.entity;

import com.slayend.fonoapp.dto.RolDTO;
import com.slayend.fonoapp.entity.Authority;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.mapper.support.AbstractHandler;

public class RolToEntityHandler extends AbstractHandler {

    @Override
    public boolean isSourceSupported(Object object) {
        return object instanceof RolDTO;
    }

    @Override
    protected <T> T doMap(Object object, Class<T> target, Mapper mapper) {
        RolDTO dto = (RolDTO) object;
        Authority entity = new Authority();
        entity.setNombre(dto.getNombre());
        return (T) entity;
    }
}
