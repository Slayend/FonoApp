package com.slayend.fonoapp.mapper.support;

import com.slayend.fonoapp.mapper.Mapper;

import java.util.*;

public abstract class AbstractMapper implements Mapper {

    @Override
    public <T> List<T> mapList(List<?> list, Class<T> itemTarget) {
        if (null == list) {
            return Collections.emptyList();
        }
        List<T> result = new ArrayList<>();

        for (Object item : list) {
            result.add(map(item, itemTarget));
        }
        return result;
    }

    public <T> Set<T> mapListToSet(List<?> list, Class<T> itemTarget) {
        if (null == list) {
            return Collections.emptySet();
        }
        Set<T> result = new HashSet<>();

        for (Object item : list) {
            result.add(map(item, itemTarget));
        }
        return result;
    }

    public <T> List<T> mapSetToList(Set<?> list, Class<T> itemTarget) {
        if (null == list) {
            return Collections.emptyList();
        }
        List<T> result = new ArrayList<>();

        for (Object item : list) {
            result.add(map(item, itemTarget));
        }
        return result;
    }

}
