package com.slayend.fonoapp.mapper.support;

import com.slayend.fonoapp.mapper.Mapper;

public interface Handler {

    void setMapper(Mapper mapper);

    <T> T map(Object object, Class<T> targetClass);

    boolean isSourceSupported(Object object);
}
