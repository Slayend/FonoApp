package com.slayend.fonoapp.mapper.support;


import com.slayend.fonoapp.mapper.Mapper;

public abstract class AbstractHandler implements Handler {

    private Mapper mapper;


    @Override
    public void setMapper(Mapper mapper) {
        this.mapper = mapper;

    }

    @Override
    public <T> T map(Object object, Class<T> targetClass) {
        if (null == object) {
            return null;
        }
        return doMap(object, targetClass, mapper);
    }

    protected abstract <T> T doMap(Object object, Class<T> target, Mapper mapper);

    public Integer evaluaLongNull(Long o) {
        if (null == o) {
            return null;
        }
        return o.intValue();
    }

    public Long evaluaIntegerNull(Integer o) {
        if (null == o) {
            return null;
        }
        return o.longValue();
    }

}
