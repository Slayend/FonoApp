package com.slayend.fonoapp.mapper.support;

import com.slayend.fonoapp.mapper.CatalogoHandlers;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MapperImpl extends AbstractMapper {

    private Map<Class<?>, List<Handler>> specificHandlers;

    public MapperImpl() {
        specificHandlers = new HashMap<>();
        CatalogoHandlers.setHandlers(this);
    }


    public void addHandler(Handler handler, Class<?>... supportedTargets) {
        for (Class<?> target : supportedTargets) {

            List<Handler> list = specificHandlers.get(target);

            if (null == list) {
                list = new ArrayList<>();
                specificHandlers.put(target, list);
            }
            handler.setMapper(this);
            list.add(handler);
        }
    }

    @Override
    public <T> T map(Object object, Class<T> target) {

        if (null == object) {
            return null;
        }

        Handler handler = resolveSpecificHandler(object, target);
        if (null == handler) {
            throw new MapperNotFoundException(object, target);
        }
        return handler.map(object, target);
    }

    private Handler resolveHandler(Object object, List<Handler> handlers) {
        if (null == handlers) {
            return null;
        }

        for (Handler handler : handlers) {
            if (handler.isSourceSupported(object)) {
                return handler;
            }
        }

        return null;
    }

    private Handler resolveSpecificHandler(Object object, Class<?> target) {
        List<Handler> handlers = specificHandlers.get(target);
        return resolveHandler(object, handlers);
    }

}
