package com.slayend.fonoapp.mapper.support;

import java.text.MessageFormat;

public class MapperNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MapperNotFoundException(Object source, Class<?> target) {
        super(MessageFormat.format("No se encuentra mapper para el objeto {0} para ser transformado en {1}", source, target));
    }
}
