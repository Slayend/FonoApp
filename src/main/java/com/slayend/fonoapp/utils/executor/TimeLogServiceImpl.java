package com.slayend.fonoapp.utils.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class TimeLogServiceImpl implements TimeLogService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeLogServiceImpl.class);

    @Override
    public <T> T executeProcess(Executor<T> executor, String processName) {
        LocalDateTime startAt = LocalDateTime.now();
        try {
            return executor.executeProcess();

        } catch (RuntimeException e) {
            LOGGER.error("error", e);
            throw e;
        } finally {
            LocalDateTime endAt = LocalDateTime.now();
            Duration duration = Duration.between(startAt, endAt);
            LOGGER.info(String.format("duracion de servicio [ %s ] inicio [ %s ]  fin [ %s ] diferencia [ %s ] ms",
                    processName, startAt, endAt, duration.toMillis()));
        }

    }

}
