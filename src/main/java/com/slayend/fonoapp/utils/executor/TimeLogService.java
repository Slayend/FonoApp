package com.slayend.fonoapp.utils.executor;

public interface TimeLogService {

    public interface Executor<T> {
        T executeProcess();
    }

    <T> T executeProcess(Executor<T> executor, String processName);


}
