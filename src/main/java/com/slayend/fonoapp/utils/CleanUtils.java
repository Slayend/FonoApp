package com.slayend.fonoapp.utils;

import io.micrometer.common.util.StringUtils;
import org.owasp.encoder.Encode;

/**
 * Metodos de limpieza detectados por Veracode.
 * La recomendacion actual es copiar la implementacion de este utilitario
 * en cada war analizado por Veracode.
 *
 * @author Gerardo Onetto [gmonetto@bancochile.cl]
 */
public class CleanUtils {

    private CleanUtils() {
    }

    public static String clean(String input) {
        return Encode.forJava(removeNewLines(input));
    }

    public static String clean(Object o) {
        return Encode.forJava(removeNewLines(toString(o)));
    }

    private static String toString(Object o) {
        return o != null ? o.toString() : null;
    }

    protected static String removeNewLines(String input) {
        if (StringUtils.isNotEmpty(input)) {
            return input.replace("\n", " ").replace("\r", " ");
        }
        return input;
    }
}
