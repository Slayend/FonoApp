package com.slayend.fonoapp.helper;


import com.slayend.fonoapp.exception.ObjectValidationException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Componente helper encargado de validar la integridad de un objeto usando las anotaciones de <b>Hibernate
 * Validator</b>.
 *
 * @author Fernando Carrasco
 */
@Component(value = "validatorHelper")
public class ValidationHelper {

    /**
     * Instancia del validador.
     */
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * Evalúa el objeto en busca de problemas de integridad, en caso que los tenga, lanzará una excepción con los
     * problemas encontrados.
     *
     * @param obj Objeto a evaluar.
     * @throws ObjectValidationException En caso de encontrarse con problemas de integridad detallará los problemas.
     */
    public <T> void validate(T obj){
        List<String> violationMessages = new ArrayList<>();
        Objects.requireNonNull(obj, "El objeto a validar no puede ser nulo");
        for (ConstraintViolation<T> violation : validator.validate(obj)) {
            violationMessages.add(violation.getMessage());
        }
        if (!violationMessages.isEmpty()) {
            throw new ObjectValidationException(violationMessages, getFormatMessage(violationMessages));
        }
    }

    /**
     * Mensaje que tendrá la excepción en caso de que ocurra un problema.
     * <br><br>
     * Ejemplo:<br> {@code Se han encontrado 3 problemas con el objeto: Problema 1, Problema 2, Problema 3}
     *
     * @param messages Lista de mensajes con los errores encontrados en los atributos de un objeto.
     * @return El mensaje final asociado a la excepción.
     * @see Throwable#getMessage()
     */
    private static String getFormatMessage(List<String> messages) {
        if (messages.size() == 1) {
            return String.format("Se ha encontrado 1 problema con el objeto: %s", appendErrorMessages(messages));
        }
        return String.format("Se han encontrado %s problemas con el objeto: %s",
                messages.size(),
                appendErrorMessages(messages));
    }

    /**
     * Concatena los errores encontrados por las validaciones.
     *
     * @param messages Lista de mensajes con los errores encontrados en los atributos de un objeto.
     * @return Los errores formateados.
     */
    private static String appendErrorMessages(List<? extends CharSequence> messages) {
        StringBuilder formattedErrors = new StringBuilder();
        int i = 0;
        while (i < messages.size() - 1) {
            formattedErrors.append(messages.get(i)).append(", ");
            i++;
        }
        return formattedErrors.append(messages.get(i)).toString();
    }

}
