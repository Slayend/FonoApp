package com.slayend.fonoapp.exception;


import com.slayend.fonoapp.helper.ValidationHelper;

import java.util.List;

/**
 * Excepción que se lanza cuando las validaciones de un objeto no se cumplen de acuerdo a {@link ValidationHelper} que
 * utiliza el framework de validaciones de Hibernate. Esta excepción además entrega ciertos detalles adicionales de
 * la cantidad y la lista de errores cuando esta fue invocada.
 *
 * @author Fernando Carrasco
 * @since 1.0.0
 */
public class ObjectValidationException extends RuntimeException {

    private static final long serialVersionUID = 1966212934828906160L;

    /**
     * Errores encontrados en el objeto evaluado.
     */
    private final int errorsAmount;

    /**
     * Lista con los errores encontrados en el objeto.
     */
    private final List<String> errors;

    /**
     * Constructor que contiene la lista de errores encontrados y un mensaje descriptivo de los errores o
     * inconsistencias que se encontraron en el objeto evaluado.
     *
     * @param errors  Lista con los errores encontrados.
     * @param message Mensaje con los errores concatenados asociados a las anotaciones.
     */
    public ObjectValidationException(List<String> errors, String message) {
        super(message);
        this.errors = errors;
        this.errorsAmount = calculate(errors);
    }

    /**
     * Calcula la cantidad de errores que provienen de la lista, si la lista viene nula, entonces retornará 0.
     *
     * @param errors Lista con los errores.
     * @return La cantidad de errores encontrados.
     */
    private int calculate(List<String> errors) {
        if (errors == null) {
            return 0;
        }
        return errors.size();
    }

    /**
     * Entrega la cantidad de errores encontrados en la lista.
     *
     * @return La cantidad de errores encontrados.
     */
    public int getErrorsAmount() {
        return errorsAmount;
    }

    /**
     * Entrega la lista de todos los errores o inconsistencias encontrados en el objeto.
     *
     * @return Lista con los errores
     */
    public List<String> getErrors() {
        return errors;
    }
}
