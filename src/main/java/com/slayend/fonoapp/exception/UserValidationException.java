package com.slayend.fonoapp.exception;

import java.io.Serial;

public class UserValidationException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 7318356034491733479L;

    public UserValidationException(String message) {
        super(message);
    }
}
