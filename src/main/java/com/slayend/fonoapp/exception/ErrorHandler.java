package com.slayend.fonoapp.exception;

import com.slayend.fonoapp.utils.CleanUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(ObjectValidationException.class)
    public ResponseEntity<ErrorInfo> methodObjectValidationException(HttpServletRequest request, ObjectValidationException e) {
        // return error info object with standard json
        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(), "El objeto posee violaciones de integridad " + CleanUtils.clean(e), request.getRequestURI());
        return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorInfo> methodRuntimeException(HttpServletRequest request, RuntimeException e) {
        return new ResponseEntity<>(getErrorInfo(request, e, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserValidationException.class)
    public ResponseEntity<ErrorInfo> methodUserValidationException(HttpServletRequest request, RuntimeException e) {
        return new ResponseEntity<>(getErrorInfo(request, e, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    private static ErrorInfo getErrorInfo(HttpServletRequest request, RuntimeException e, int status) {
        return new ErrorInfo(status, e.getMessage(), request.getRequestURI());
    }
}
