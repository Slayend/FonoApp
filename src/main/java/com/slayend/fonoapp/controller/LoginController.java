package com.slayend.fonoapp.controller;

import com.slayend.fonoapp.dto.UserDTO;
import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.repository.ICustomersRepository;
import com.slayend.fonoapp.service.LoginService;
import com.slayend.fonoapp.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {


    @Autowired
    private ICustomersRepository customersRepository;

    @Autowired
    private Mapper mapper;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private LoginService loginService;

    @PostMapping("/register")
    public UserDTO registerUser(@RequestBody UserDTO userDTO) {
        validationService.isValid(userDTO);
        return loginService.CrearUsuario(userDTO);
    }

    @RequestMapping("/user")
    public UserDTO getUserDetailsAfterLogin(Authentication authentication) {
        Customer customers = customersRepository.findByEmail(authentication.getName());
        if (customers != null) {
            UserDTO userDTO = mapper.map(customers, UserDTO.class);
            return userDTO;
        } else {
            return null;
        }

    }
}
