package com.slayend.fonoapp.repository;

import com.slayend.fonoapp.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICustomersRepository extends JpaRepository<Customer, Long> {


    Customer findByEmail(String email);

    boolean existsByEmail(String email);

}
