package com.slayend.fonoapp.constants;

public interface SecurityConstants {

    /**
     * este valor que es la key del token debe ser inyectada como constante de ambiente
     */
    public static final String JWT_KEY = "jxgEQeXHuPq8VdbyYFNkANdudQ53YUn4";
    public static final String JWT_HEADER = "Authorization";

}
