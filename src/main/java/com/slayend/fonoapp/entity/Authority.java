package com.slayend.fonoapp.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "AUTORITY")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Authority {

    @Id
    @Column(name = "ID_AUTHORITY")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nombre")
    @NotBlank(message = "Nombre no puede estar vasio")
    @NotNull(message = "Nombre no puede ser null")
    private String nombre;
    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "customer_id")
    private Customer customer;

}
