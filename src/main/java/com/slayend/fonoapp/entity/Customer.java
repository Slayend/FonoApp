package com.slayend.fonoapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Customer {
    @Id
    @Column(name = "ID_CUSTOMER")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "EMAIL", unique = true)
    @NotNull(message = "Customer.email No puede ser null")
    @NotBlank(message = "Customer.email No puede estar vacio")
    private String email;

    @NotNull(message = "Customer.username No puede ser null")
    @NotBlank(message = "Customer.username No puede estar vacio")
    @Column(name = "USERNAME")
    private String username;

    @NotNull(message = "Customer.password No puede ser null")
    @NotBlank(message = "Customer.password No puede estar vacio")
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLE")
    private int enabled;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Authority> authorities;
}