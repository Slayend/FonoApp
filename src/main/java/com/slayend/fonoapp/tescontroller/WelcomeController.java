package com.slayend.fonoapp.tescontroller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    @GetMapping("/welcome")
    public String sayWelcome() {
        return "welcome to application Spring security";
    }

    @GetMapping("/welcome2")
    public String sayWelcome2() {
        return "welcome to application Spring security";
    }

    @GetMapping("/welcome3")
    public String sayWelcome3() {
        return "llamada con usuario admin";
    }

}
