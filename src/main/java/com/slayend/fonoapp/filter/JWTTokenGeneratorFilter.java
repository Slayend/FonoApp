package com.slayend.fonoapp.filter;

import com.slayend.fonoapp.constants.SecurityConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * al extender de OncePerRequestFilter le decimos a spring security que se ejecute solo una vez por llamada a este filtro.
 * este filtro se encargara de generar el token
 */
public class JWTTokenGeneratorFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        //obtenemos los detalles del usuario autenticado
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (null != authentication) {
            //generamos la secretKey para firmar el jwt
            SecretKey key = Keys.hmacShaKeyFor(SecurityConstants.JWT_KEY.getBytes(StandardCharsets.UTF_8));
            //generamos el jwt
            String jwt = Jwts.builder().setIssuer("FonoApp").setSubject("JWT Token")
                    //agregamos el username del usuario al jwt
                    .claim("username", authentication.getName())
                    //agregamos los roles del usuario al jwt
                    .claim("authorities", populateAuthorities(authentication.getAuthorities()))
                    //definimos cuando se creo el token para posterior realizar validacion de tiempo de session
                    .setIssuedAt(new Date())
                    //le indicamo cuanto es el tiempo de caducidad del jwt
                    .setExpiration(new Date((new Date()).getTime() + 30000000))
                    //firmamos el jwt para esto le pasamos la secret key
                    .signWith(key).compact();
            response.setHeader(SecurityConstants.JWT_HEADER, jwt);
        }

        filterChain.doFilter(request, response);
    }

    /**
     * con esta implementacion de shouldNotFilter indicamos cuando queremos que se ejecute este filtro en
     * este caso solo queremos que se ejecute cuando tratamos de inciar sesion para no estar generando un token
     * cada vez que hacemos alguna peticion al backEnd
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !request.getServletPath().equals("/user");
    }

    private String populateAuthorities(Collection<? extends GrantedAuthority> collection) {
        Set<String> authoritiesSet = new HashSet<>();
        for (GrantedAuthority authority : collection) {
            authoritiesSet.add(authority.getAuthority());
        }
        return String.join(",", authoritiesSet);
    }

}
