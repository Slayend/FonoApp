package com.slayend.fonoapp.configuration.security;

import com.slayend.fonoapp.filter.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;


@Configuration
public class SpringSecurityConfig {

    private static final String[] AUTH_WHITELIST = {
            "/authenticate",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/v3/api-docs",
            "/v2/api-docs/**",
            "/webjars/**",
            "/welcome",
            "/register",
            "/v2/**",
            "/configuration/**"
    };


    /**
     * A partir de Spring Security 5.7, WebSecurityConfigurerAdapter está obsoleto
     * para animar a los usuarios para avanzar hacia una configuración de seguridad
     * basada en componentes. Se recomienda crear un bean de tipo
     * SecurityFilterChain para configuraciones relacionadas con la seguridad.
     *
     * @return SecurityFilterChain Excepción @throws
     * @paramhttp
     */

    // define las urls que pueden acceder con autenticacion y sin autenticación
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {

        /**
         * Configuraciones predeterminadas que asegurarán todas las solicitudes
         */
        /*
         * ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)http.authorizeRequests()
         * .anyRequest()). authenticated(); http.formLogin(); http.httpBasic(); return
         * (SecurityFilterChain)http.build();
         */

        /**
         * Configuraciones personalizadas según nuestro requisito
         *
         * se agrega los cors para la conxion de origenes diferentes en este caso para unir
         * el front con el back ya que se encuentran en servidores distintos y rutas distintas
         *
         * se agrega el gestor de sesiones para indicar que nosotros nos prepcuparemos de las sessiones para integrar jwt
         */
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .cors().configurationSource(new CorsConfigurationSource() {
                    @Override
                    public CorsConfiguration getCorsConfiguration(HttpServletRequest request) {
                        CorsConfiguration config = new CorsConfiguration();
                        /**
                         * Lista separada por comas de orígenes incluidos en la lista blanca o “*”.
                         * Le permite especificar una lista de orígenes permitidos. Por defecto, permite todos los orígenes.
                         */
                        //config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
                        /**
                         * Lista separada por comas de métodos HTTP que el servidor web permite para solicitudes de origen cruzado.
                         */
                        config.setAllowedMethods(Collections.singletonList("*"));

                        /**
                         * Si el navegador realiza una solicitud al servidor pasando credenciales
                         * (en forma de cookies o encabezados de autorización), su valor se establece en true.
                         * Cuando se requieran credenciales para invocar la API, establezca en verdadero.
                         * En caso de que no se requieran credenciales, omita el encabezado.
                         */
                        config.setAllowCredentials(true);
                        /**
                         * Lista separada por comas de encabezados HTTP que el servidor web permite para solicitudes de origen cruzado.
                         */
                        config.setAllowedHeaders(Collections.singletonList("*"));

                        /**
                         * exponemos el header que contendra el jwt
                         */
                        config.setExposedHeaders(Arrays.asList("Authorization"));
                        /**
                         * Indica cuánto tiempo se pueden almacenar en caché los resultados de una solicitud de verificación previa.
                         * El valor predeterminado maxAgees 1800 segundos (30 minutos).
                         * Indica cuánto tiempo se pueden almacenar en caché las respuestas de verificación previa.
                         */
                        config.setMaxAge(3600L);
                        return config;
                    }
                }).and().
                /* csrf se utiliza para asegurar las peticiones con un token guardado en las COOKIE
                 */
                        csrf()
                /**
                 * ignoringAntMatchers se utiliza para ignarar el csrf de dichas rutas
                 */
                .ignoringRequestMatchers("/**")
                /**
                 * csrfTokenRepository  se utiliza para indicar a spring que genere un token de referencia crsf
                 * y envie ese token como una cookie a mi aplicacion
                 * withHttpOnlyFalse se utiliza para dejar el parametro en falso para que el navegador tambien pueda
                 * leer la cookie
                 */
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and()
                /**
                 * los filtros son utilizado por spring security para ejecutar la seguridad de esta y nosotros podemos agrega nuevos filtros
                 * para aplicar nuestras propias reglas
                 */
                //este filtro se ejecutara antes del filtro basicAuthenticationFilter de spring security
                //.addFilterBefore(new RequestValidationBeforeFilter(), BasicAuthenticationFilter.class)
                //este filtro se ejecutara antes o despues de BasicAuthenticationFilter ya que le estamos indicando que se ejecute en la misma posicion
                //.addFilterAt(new AuthoritiesLoggingAtFilter(), BasicAuthenticationFilter.class)
                //este filtro se ejecutara despues de BasicAuthenticationFilter
                //.addFilterAfter(new AuthoritiesLoggingAfterFilter(), BasicAuthenticationFilter.class)
                //.addFilterAfter(new JWTTokenGeneratorFilter(), BasicAuthenticationFilter.class)
                //.addFilterBefore(new JWTTokenValidatorFilter(), BasicAuthenticationFilter.class)
                .authorizeHttpRequests((auth) -> auth
                        //estas rutas necesitan ser llamadas por un usuario con el rol admin
                        //.requestMatchers("/welcome3").hasAuthority("ADMIN")
                        //        		.antMatchers("/welcome3").hasRol("ADMIN")
                        //.antMatchers("/autenticate").hasRole("Adminn")
                        //estas rutas necesitan autenticación para ser llamadas
                        //.requestMatchers("/myBalance", "/myLoans", "/myCards", "/welcome2", "/user").authenticated()
                        //estas rutas son accesibles por todos
                        .requestMatchers("/**").permitAll())
                //genera un html con un login por defecto
                .formLogin().and()
                //indica el tipo de seguridad
                .httpBasic(Customizer.withDefaults());
        return http.build();

        /**
         * Configuración para denegar todas las solicitudes
         */
        /*
         * http.authorizeHttpRequests( (auth)->auth .anyRequest().denyAll())
         * .httpBasic(Customizer.withDefaults()); return http.build();
         */

        /**
         * Configuración para permitir todas las solicitudes
         */
        /*
         * http.authorizeHttpRequests( (auth)->auth .anyRequest().permitAll())
         * .httpBasic(Customizer.withDefaults()); return http.build();
         */
    }


    //se definen los usuarios en memoria que puede acceder a spring security
	
/*
		 @Bean
		 public InMemoryUserDetailsManager userDetailsService() {
			
			Enfoque 1 donde usamos el método withDefaultPasswordEncoder() al crear
			los datos del usuario
			 

			 UserDetails admin = User.withUsername("admin")
			 .password("12345").authorities("admin") .build(); 
			 
			 UserDetails user =User.withUsername("user").password("12345")
			 .authorities("read").build(); 
			 return new InMemoryUserDetailsManager(admin,user);
		
			 
			 Enfoque 2 donde no definimos el codificador de contraseña al crear el usuario
			 detalles. En su lugar, se creará un bean PasswordEncoder independiente.
			 
			 InMemoryUserDetailsManager userDetailsService = new InMemoryUserDetailsManager(); 
			 UserDetails admin = User.withUsername("admin").password("12345").authorities("admin").build();
			 UserDetails user = User.withUsername("user").password("12345").authorities("read").build();
			 userDetailsService.createUser(admin); 
			 userDetailsService.createUser(user);
			 return userDetailsService; 
				 
		 }

		 
			//es utilizado para decir que usarmos la tabla user definida por spring security
		 @Bean 
		 public UserDetailsService userDetailsService(DataSource dataSource) {
			 return new JdbcUserDetailsManager(dataSource); 
		 }
		 
*/

    /**
     * Metodo utilizado para codifcar contraseña para iniciar sesión
     */

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
