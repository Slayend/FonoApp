package com.slayend.fonoapp.service;


import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.exception.ObjectValidationException;

/**
 * Servicio de validaciones JSR 303 para los objetos que contengan datos obligatorios y poder identificarlos.
 *
 * @author Fernando Carrasco
 */
public interface ValidationService {

    /**
     * Consulta si el objeto validado, posee o no violaciones de integridad.
     *
     * @param obj Objeto a evaluar.
     * @param <T> Tipo de objeto (sin restricción) a evaluar.
     * @return TRUE: Si el objeto no posee violaciones de integridad; FALSE: En caso contrario.
     */
    <T> boolean isValid(T obj);

    boolean validacionPersonalizada(Customer customer);
}
