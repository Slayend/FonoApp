package com.slayend.fonoapp.service.impl;

import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.exception.ObjectValidationException;
import com.slayend.fonoapp.helper.ValidationHelper;
import com.slayend.fonoapp.service.ValidationService;
import com.slayend.fonoapp.utils.CleanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Implementación del servicio de validaciones.
 *
 * @author Fernando Carrasco
 */
@Slf4j
@Service
public class ValidationServiceImpl implements ValidationService {

    @Autowired
    private ValidationHelper validationHelper;

    /**
     * Consulta si el objeto validado, posee o no violaciones de integridad.
     *
     * @param obj Objeto a evaluar.
     * @return TRUE: Si el objeto no posee violaciones de integridad; FALSE: En caso contrario.
     */
    @Override
    public <T> boolean isValid(T obj) {
        this.validationHelper.validate(obj);
        return true;
    }

    @Override
    public boolean validacionPersonalizada(Customer customer) {
        try {

        } catch (Exception e) {
            log.error("Error en campos obligatorios {} {}", CleanUtils.clean(customer), CleanUtils.clean(e));
            //lanzar un throw
        }
        return true;
    }

}

