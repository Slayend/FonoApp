package com.slayend.fonoapp.service.impl;

import com.slayend.fonoapp.dto.UserDTO;
import com.slayend.fonoapp.entity.Authority;
import com.slayend.fonoapp.entity.Customer;
import com.slayend.fonoapp.exception.UserValidationException;
import com.slayend.fonoapp.mapper.Mapper;
import com.slayend.fonoapp.repository.ICustomersRepository;
import com.slayend.fonoapp.service.LoginService;
import com.slayend.fonoapp.utils.executor.TimeLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private ICustomersRepository customersRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Mapper mapper;

    @Autowired
    private TimeLogService timeLogService;

    @Override
    public UserDTO CrearUsuario(UserDTO userDTO) {
        if (customersRepository.existsByEmail(userDTO.getEmail())) {
            log.error("Error ya existe este usuario " + userDTO.getEmail());
            throw new UserValidationException("Error ya existe este usuario " + userDTO.getEmail());
        }
        Customer customer = mapper.map(userDTO, Customer.class);
        String hashPwd = passwordEncoder.encode(customer.getPassword());
        customer.setPassword(hashPwd);
        customer.setAuthorities(getRols(customer));
        customer.setEnabled(1);
        return mapper.map(timeLogService.executeProcess(() -> customersRepository.save(customer), "ICustomersRepository.save"), UserDTO.class);
    }

    private static Set<Authority> getRols(Customer customer) {
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setNombre("ROLE_USER");
        authority.setCustomer(customer);
        authorities.add(authority);
        return authorities;
    }
}
