package com.slayend.fonoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.CrossOrigin;



@CrossOrigin(origins = "*")
@SpringBootApplication
@EnableWebSecurity(debug = true) //NO USAR EN PRODUCCIÓN
public class FonoappApplication {

	public static void main(String[] args) {
		SpringApplication.run(FonoappApplication.class, args);
		

	}

}
